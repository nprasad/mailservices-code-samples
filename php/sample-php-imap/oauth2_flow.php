<?php

# Implement this:
# https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-device-code

$tenant_id = "c80d3499-4a40-4a8c-986e-abce017d6b19";
$client_id = "543725ae-fd38-436a-a717-009b1a8137be";
$scope = "https://outlook.office.com/IMAP.AccessAsUser.All https://outlook.office.com/POP.AccessAsUser.All https://outlook.office.com/SMTP.Send offline_access";

// Cache file for refresh_token
$refresh_token_cache_file = "/tmp/refresh_token.cache";

// https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-device-code#device-authorization-request
function device_authorization_request($tenant_id, $client_id, $scope)
{
        $postData = array(
                'client_id' => $client_id,
                'scope' => $scope
                );
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, "https://login.microsoftonline.com/" . $tenant_id . "/oauth2/v2.0/devicecode");
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($postData));
        $output = curl_exec($c);
        $device_code = null;

        if($output === false)
        {
                $error = 'Error curl : ' . curl_error($c);
                echo $error . "\n";
        }
        else
        {
                $results = json_decode($output);
                echo "==============================================================================================================================\n";
                echo $results->message;
                echo "\n==============================================================================================================================\n";

                $device_code = $results->device_code;
        }
        curl_close($c);

        return $device_code;
}

// https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-device-code#authenticating-the-user
function authenticating_the_user($tenant_id, $client_id, $device_code)
{
        $postData = array(
                'grant_type' => "urn:ietf:params:oauth:grant-type:device_code",
                'client_id' => $client_id,
                'device_code' => $device_code
                );

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, "https://login.microsoftonline.com/" . $tenant_id . "/oauth2/v2.0/token");
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($postData));
        $output = curl_exec($c);
        $token_response = null;
        if($output === false)
        {
                $error = 'Error curl : ' . curl_error($c);
                echo $error . "\n";
        }
        else
        {
                $token_response = json_decode($output);
                // echo $output . "\n\n";

        }
        curl_close($c);
        return $token_response;
}

// https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow#refresh-the-access-token
function refreshing_the_token($tenant_id, $client_id, $scope, $refresh_token)
{
        $postData = array(
                'grant_type' => "refresh_token",
                'client_id' => $client_id,
                'refresh_token' => $refresh_token,
                'scope' => $scope
                );

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, "https://login.microsoftonline.com/" . $tenant_id . "/oauth2/v2.0/token");
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($postData));
        $output = curl_exec($c);
        $token_response = null;
        if($output === false)
        {
                $error = 'Error curl : ' . curl_error($c);
                echo $error . "\n";
        }
        else
        {
                $token_response = json_decode($output);
                // echo $output . "\n\n";
        }
        curl_close($c);
        return $token_response;
}

function get_access_token()
{
        global $tenant_id, $client_id, $scope, $refresh_token_cache_file;

        $token_response = null;

        // Check if cache file is there, and use it
        if (file_exists($refresh_token_cache_file)) {
                echo "refresh_token found, trying to renew...\n";
                $refresh_token = file_get_contents($refresh_token_cache_file);
                $token_response = refreshing_the_token($tenant_id, $client_id, $scope, $refresh_token);
        }

        // Refreshing the token failed, (re)start from scratch
        if (($token_response === null) || !isset($token_response->token_type) || ($token_response->token_type !== "Bearer"))
        {
                echo "Missing or invalid refresh_token, restarting device code flow...\n";
                $device_code = device_authorization_request($tenant_id, $client_id, $scope);
                if ($device_code !== null) {
                        // Poll the token Endpoint until the user authenticated.
                        $token_response_timeout = 0;
                        $token_response = null;
                        while (($token_response === null) || (isset($token_response->error) && ($token_response->error === "authorization_pending"))) 
                        {
                                sleep(10);
                                $token_response = authenticating_the_user($tenant_id, $client_id, $device_code);
                                $token_response_timeout += 10;
                                if ($token_response_timeout > 5*60) // Timeout after 5 minutes.
                                {
                                        echo "Timeout waiting for token_response. Please try again.\n";
                                        break;
                                }
                        }
                }
        }

        if (isset($token_response->token_type) && ($token_response->token_type === "Bearer"))
        {
                echo "Authenticated, access_token retrieved\n";
                // Saving refresh_token to cache file
                file_put_contents($refresh_token_cache_file, $token_response->refresh_token, LOCK_EX);
                chmod($refresh_token_cache_file, 0600);
                // Return full token so both username and access_token can be used.
                return $token_response->access_token;
        }

        return null;
}

function get_username_from_access_token($access_token)
{
        $jwt_decoded = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $access_token)[1]))));
        // echo $jwt_decoded->upn . "\n";
        return $jwt_decoded->upn;
}

// Demo it
// $zz = get_access_token();
// get_username_from_access_token($zz);
// echo "\n";

?>
