<?php
require __DIR__ . '/vendor/autoload.php';
use Webklex\PHPIMAP\ClientManager;
use Webklex\PHPIMAP\Client;

include("oauth2_flow.php");

$imap_server = "{outlook.office365.com:993/imap/ssl}";

// Aquire token
$access_token = get_access_token();
// Extract username from token
$username = get_username_from_access_token($access_token);

$cm = new ClientManager($options = []);
$client = $cm->make([
'host'          => 'outlook.office365.com',
'port'          => 993,
'encryption'    => 'ssl',
'validate_cert' => true,
'username'      => $username,
'password'      => $access_token,
'protocol'      => 'imap',
'authentication'=> "oauth"
]);

$client->connect();

//print("Connected:".$client->isConnected()."\n");

$inbox = $client->getFolderByPath('INBOX');

$messages = $inbox->messages()->all()->get();
foreach ($messages as $msg) {
    echo "\n=============\n";
    echo $msg->getSubject();
    echo "\n---------------\n";
    echo $msg->getTextBody();
    echo "\n=============\n";
}

?>