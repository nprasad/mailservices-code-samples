<?php
include("oauth2_flow.php");
require __DIR__ . '/vendor/autoload.php';

$imap_server = "{outlook.office365.com:993/imap/ssl}";

// Aquire token
$access_token = get_access_token();
// Extract username from token
$username = get_username_from_access_token($access_token);

$mbox = imap2_open($imap_server, $username, $access_token, OP_XOAUTH2);
if (! $mbox) {
    error_log(imap_last_error());
    throw new RuntimeException('Unable to open CERN INBOX on 365');
}

$check = imap2_mailboxmsginfo($mbox);
print_r($check);

$folders = imap2_listmailbox($mbox, $imap_server, "*");
if ($folders == false) {
    echo "Call failed\n";
} else {
    foreach ($folders as $val) {
        echo $val . "\n";
    }
}

imap2_close($mbox);
?>