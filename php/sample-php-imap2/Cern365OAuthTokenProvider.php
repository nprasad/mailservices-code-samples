<?php
namespace PHPMailer\PHPMailer;

class Cern365OAuthTokenProvider implements OAuthTokenProvider
{
    protected $username = null;
    protected $access_token = null;

    public function __construct($options)
    {
        $this->username = $options['username'];
        $this->access_token = $options['access_token'];
    }

    // The string to be base 64 encoded should be in the form: 
    // "user=<user_email_address>\001auth=Bearer <access_token>\001\001"
    public function getOauth64()
    {
        if (($this->username !== null) && ($this->access_token !== null)) {
            return base64_encode("user={$this->username}\001auth=Bearer {$this->access_token}\001\001");
        }
        return null;
    }
}
?>