"""Process incoming emails."""
import time
import base64
from oauth2_pop.config import Config
from oauth2_pop.oauth2_flow import Oauth2Flow
import poplib

class PopClient:
    """Process incoming emails."""

    def __init__(self, conf: Config) -> None:
        """Initialize the Pop client."""
        self.conf = conf
        self.oauth = Oauth2Flow(conf)

    def process_mailbox(self) -> None:
        """Process mailbox pop access"""
        while True:
            print("Fetching emails...")
            self.__process_standard()
            time.sleep(30)

    def __process_standard(self) -> None:
        """Process normal poplib authentication"""
        # Authenticate to account using OAuth 2.0 mechanism
        access_token, username = self.oauth.get_access_token()
        # 365 POP needs base64 encoded token, set to True.
        auth_string = self.sasl_xoauth2(username, access_token, True)
        print("username", username)
        pop_conn = poplib.POP3_SSL(self.conf.POP_SERVER)
        pop_conn.set_debuglevel(True)
        pop_conn.getwelcome()
        pop_conn._shortcmd('AUTH XOAUTH2')
        pop_conn._shortcmd(auth_string)
        pop_conn.stat()
        pop_conn.quit()

    def sasl_xoauth2(self, username, access_token, base64_encode=False) -> str:
        """Convert the access_token into XOAUTH2 format"""
        auth_string = "user=%s\1auth=Bearer %s\1\1" % (username, access_token)
        if base64_encode:
            auth_string = base64.b64encode(auth_string.encode("ascii")).decode("ascii")
        return auth_string
