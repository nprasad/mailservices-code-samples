"""oauth2 samples Entrypoint."""
from oauth2_pop.config import load_config
from oauth2_pop.pop_client import PopClient


if __name__ == "__main__":
    conf = load_config()
    z = PopClient(conf)
    z.process_mailbox()
