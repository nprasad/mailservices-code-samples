"""imap-oauth2 Entrypoint."""
from oauth2_smtp.config import load_config
from oauth2_smtp.smtp_client import SmtpClient


if __name__ == "__main__":
    conf = load_config()
    z = SmtpClient(conf)
    z.process_smtp()
