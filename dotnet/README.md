# .NET XOauth2 sample
- Using Microsoft ```msal``` library.
- OpenPOP library was modified to support XOauth2 authentication.
- Original Microsoft samples: 
    - [Device Code Flow](https://github.com/Azure-Samples/ms-identity-dotnet-desktop-tutorial/tree/master/4-DeviceCodeFlow)
    - [Token Cache](https://github.com/Azure-Samples/ms-identity-dotnet-desktop-tutorial/tree/master/2-TokenCache)
- [Official documentation](https://docs.microsoft.com/en-us/exchange/client-developer/legacy-protocols/how-to-authenticate-an-imap-pop-smtp-application-by-using-oauth)
