package ch.cern.app;

import com.microsoft.aad.msal4j.ITokenCacheAccessAspect;
import com.microsoft.aad.msal4j.PublicClientApplication;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.aad.msal4j.IAccount;
import com.microsoft.aad.msal4j.InteractiveRequestParameters;
import com.microsoft.aad.msal4j.SilentParameters;
import com.microsoft.aad.msal4j.DeviceCode;
import com.microsoft.aad.msal4j.DeviceCodeFlowParameters;

import java.util.Set;
import java.net.URI;
import java.util.HashSet;
import java.util.Arrays;
import java.util.function.Consumer;

// Process Oauth2 authentication flow.
public class Oauth2Flow {
    private ITokenCacheAccessAspect persistenceAspect;

    Oauth2Flow() {
        // Initialize and Load the cache if any
        persistenceAspect = new TokenPersistence(Config.REFRESH_TOKEN_CACHE_FILE);
    }

    public IAuthenticationResult getAccessToken() {
        // Create a preferably long-lived app instance which maintains a token cache.
        PublicClientApplication app = null;
        try {
            app = PublicClientApplication
                .builder(Config.AZURE_CLIENT_ID)
                .authority(Config.AZURE_AUTHORITY)
                .setTokenCacheAccessAspect(persistenceAspect)
                .build();
        } catch (Exception ex) {
            System.out.println("Error acquireTokenSilently: " + ex.getMessage());
        }

        // Try to reload token from the cache
        Set<IAccount> accountsInCache = app.getAccounts().join();
        // Take first account in the cache.
        IAccount account = null;
        try {
            account = accountsInCache.iterator().next();
        } catch (Exception ex) {
            System.out.println("Error, no accounts found: " + ex.getMessage());
        }

        IAuthenticationResult result = null;
        if (account != null) {
            // System.out.println("Account: " + account);
            try {
                SilentParameters silentParameters =
                    SilentParameters
                            .builder(new HashSet<>(Arrays.asList(Config.AZURE_SCOPE)), account)
                            .build();
                result = app.acquireTokenSilently(silentParameters).join();
            } catch (Exception ex) {
                System.out.println("Error acquireTokenSilently: " + ex.getMessage());
            }
        }

        if (result == null) {
            System.out.println("No suitable token exists in cache. Let's get a new one from AAD.");
            try {
                //  Consumer<DeviceCode> deviceCodeConsumer = (DeviceCode deviceCode) ->
                //         System.out.println(deviceCode.message());

                Consumer<DeviceCode> deviceCodeConsumer = new Consumer<DeviceCode>() {
                    @Override
                    public void accept(DeviceCode deviceCode) {
                        System.out.println(deviceCode.message());
                    }
                };
                
                DeviceCodeFlowParameters parameters =
                        DeviceCodeFlowParameters
                                .builder(new HashSet<>(Arrays.asList(Config.AZURE_SCOPE)), deviceCodeConsumer)
                                .build();

                // Try to acquire a token via device code flow.
                result = app.acquireToken(parameters).join();
            } catch (Exception ex) {
                System.out.println("Error acquireToken: " + ex.getMessage());
            }
        }

        // System.out.println("result:");
        // System.out.println(result.accessToken());
        // System.out.println(result.account().username());
        return(result);
    }
}