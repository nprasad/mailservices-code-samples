package ch.cern.app;

import jakarta.mail.*;
import java.util.Properties;

public class ImapClient {

   public void ImapConnect(String username, String accessToken) {
        // https://javaee.github.io/javamail/OAuth2
         Properties props = new Properties();
         props.put("mail.imap.ssl.enable", "true"); // required 
         props.put("mail.imap.auth.mechanisms", "XOAUTH2");
         try {
            Session session = Session.getInstance(props);
            Store store = session.getStore("imap");
            store.connect(Config.IMAP_SERVER, username, accessToken);

            Folder[] folders = store.getDefaultFolder().list("*");
            for (Folder folder : folders) {
               if ((folder.getType() & Folder.HOLDS_MESSAGES) != 0) {
                  System.out.println(folder.getFullName() + ": " + folder.getMessageCount());
               }
            }
            store.close();

         } catch (Exception ex) {
                System.out.println("Error ImapConnect: " + ex.getMessage());
         }
   }
}

