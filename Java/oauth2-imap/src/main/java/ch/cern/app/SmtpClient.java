package ch.cern.app;

import jakarta.mail.*;
import jakarta.mail.internet.*;
import java.util.Properties;

public class SmtpClient {

   public void SmtpConnect(final String username, final String accessToken) {
        // https://javaee.github.io/javamail/OAuth2
         Properties props = new Properties();
         props.put("mail.smtp.host", Config.SMTP_SERVER);
         props.put("mail.smtp.port", "587"); //TLS Port ??
         props.put("mail.smtp.starttls.enable", "true"); // required 
         props.put("mail.smtp.starttls.required", "true");
         props.put("mail.smtp.auth", "true"); //enable authentication
         props.put("mail.smtp.auth.mechanisms", "XOAUTH2");
         props.put("mail.smtp.localhost", "javamail.cern.ch"); // required, or localhost will be used and might fail with error 501 5.5.4 Invalid domain name
         // props.put("mail.debug", "true");
         try {
            Session session = Session.getInstance(props,
               new Authenticator() {
                  protected PasswordAuthentication getPasswordAuthentication() {
                     return new PasswordAuthentication(username, accessToken);
                  }
               });

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(username));
            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("noreply@cern.ch")); // UPDATE ACCORDINGLY
            // Set Subject: header field
            message.setSubject("This is the Subject Line!");
            // Now set the actual message
            message.setText("This is actual message");
            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");

         } catch (Exception ex) {
                System.out.println("Error SmtpConnect: " + ex.getMessage());
         }
   }
}

