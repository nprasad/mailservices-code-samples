package ch.cern.app;

public class Config
{
    public static String AZURE_AUTHORITY = "https://login.microsoftonline.com/cern.ch";
    public static String AZURE_CLIENT_ID = "543725ae-fd38-436a-a717-009b1a8137be";
    public static String[] AZURE_SCOPE = {
        "https://outlook.office.com/IMAP.AccessAsUser.All",
        "https://outlook.office.com/POP.AccessAsUser.All",
        "https://outlook.office.com/SMTP.Send",
    };

    public static String IMAP_SERVER = "outlook.office365.com";
    public static String POP_SERVER = "outlook.office365.com";
    public static String SMTP_SERVER = "outlook.office365.com";

    public static String REFRESH_TOKEN_CACHE_FILE = "/tmp/refresh_token.cache";
}