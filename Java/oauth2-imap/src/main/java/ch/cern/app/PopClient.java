package ch.cern.app;

import jakarta.mail.*;
import java.util.Properties;

public class PopClient {

   public void PopConnect(String username, String accessToken) {
        // https://javaee.github.io/javamail/OAuth2
         Properties props = new Properties();
         props.put("mail.pop3.ssl.enable", "true"); // required 
         props.put("mail.pop3.auth.mechanisms", "XOAUTH2");
         // true for MS 365, false for Google
         props.put("mail.pop3.auth.xoauth2.two.line.authentication.format", "true");
         try {
            Session session = Session.getInstance(props);
            Store store = session.getStore("pop3");
            
            store.connect(Config.POP_SERVER, username, accessToken);

            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);
            System.out.println("INBOX Size: " + inbox.getMessageCount());
            inbox.close(false);

            store.close();

         } catch (Exception ex) {
                System.out.println("Error PopConnect: " + ex.getMessage());
         }
   }
}

